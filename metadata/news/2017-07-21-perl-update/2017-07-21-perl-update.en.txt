Title: Perl 5.26 update
Author: Heiko Becker <heirecka@exherbo.org>
Content-Type: text/plain
Posted: 2017-07-21
Revision: 2
News-Item-Format: 1.0
Display-If-Installed: dev-lang/perl[<5.26.0]

Perl has been bumped to 5.26.0

To switch to that version, run...

    # eclectic perl set 5.26

As always after changing perl slots, you will need to rebuild all packages
that install Perl modules. To do this, you can run...

    # cave resolve -1x $(cave print-owners -f '%I ' /usr/${CHOST}/lib/perl5/vendor_perl/5.24-arch )
    # cave resolve -1x $(cave print-owners -f '%I ' /usr/${CHOST}/lib/perl5/vendor_perl/5.24-pure )

Replacing ${CHOST} with the triplet matching your system, and executing
those commands for any cross targets which Perl is installed to (or even
older perl slots).

In addition, if you installed any site-modules manually, you have to
reinstall those as well.
